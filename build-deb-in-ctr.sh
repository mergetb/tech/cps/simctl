#!/bin/bash

set -e

docker build -f debian/builder.dock -t simctl-builder .
docker run -v `pwd`:/simctl simctl-builder /simctl/build-deb.sh

