VERSION = $(shell git describe --always --long --dirty --tags)
LDFLAGS = "-X gitlab.com/mergetb/tech/cps/simctl/api.Version=$(VERSION)"
DEBVER = $(shell head -1 debian/changelog | grep -oP '\d+\.\d+\.\d+-\d+')

.PHONY: all
all: build/simd \
	 build/simc \
	 build/simctld \
	 build/simctl

# main components -------------------------------------------------------------

build/simd: simd/main.go simd/*.go api/simctl.pb.go api/*.go
	$(go-build)

build/simc: simc/main.go simc/*.go api/simctl.pb.go api/*.go
	$(go-build)

build/simctld: simctld/main.go api/simctl.pb.go api/*.go
	$(go-build)

build/simctl: simctl/main.go api/simctl.pb.go api/*.go
	$(go-build)

# protobuf --------------------------------------------------------------------

api/simctl.pb.go: api/simctl.proto
	$(protoc-build)
	$(QUIET) sed -r -i \
		's/json:"(.*)"/json:"\1" yaml:"\1" mapstructure:"\1"/g' api/simctl.pb.go

# container -------------------------------------------------------------------

REGISTRY ?= docker.io
REPO ?= mergetb
TAG ?= latest
BUILD_ARGS ?= --no-cache

.PHONY: $(REGISTRY)/$(REPO)/simctld
$(REGISTRY)/$(REPO)/simctld: container/simctld.dock build/simctld
	$(docker-build)

.PHONY: containers
containers: $(REGISTRY)/$(REPO)/simctld

build/simd_$(DEBVER)_amd64.deb: build/simd
	./build-deb-in-ctr.sh

# util ------------------------------------------------------------------------

.PHONY: clean
clean:
	rm -rf build

.PHONY: distclean
distclean: clean
	find . -name *.pb.go -delete
	rm -rf .tools

.tools:
	$(QUIET) mkdir .tools

protoc-gen-go=.tools/protoc-gen-go
$(protoc-gen-go): | .tools
	$(QUIET) GOBIN=`pwd`/.tools go install \
		github.com/golang/protobuf/protoc-gen-go

.PHONY: tools
tools: $(protoc-gen-go)

.PHONY: install
install: build/simd build/simc build/simctl build/simctld
	install -D build/simd    $(DESTDIR)$(prefix)/bin/simd
	install -D build/simc    $(DESTDIR)$(prefix)/bin/simc
	install -D build/simctl  $(DESTDIR)$(prefix)/bin/simctl
	install -D build/simctld $(DESTDIR)$(prefix)/bin/simctld

include merge.mk
