package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"go.etcd.io/bbolt"
)

const (
	dbPath     = "/var/run/simd/bolt.db"
	simdBucket = "simd"
	simctr     = "simctr"
)

func initDb() error {

	db, err := bbolt.Open(dbPath, 0600, &bbolt.Options{Timeout: 5 * time.Second})
	if err != nil {
		return errors.WithStack(err)
	}
	defer db.Close()

	return db.Batch(func(tx *bbolt.Tx) error {

		b, err := tx.CreateBucketIfNotExists([]byte(simdBucket))
		if err != nil {
			return errors.WithStack(err)
		}

		buf := b.Get([]byte(simctr))
		if buf == nil {
			err := initSimCounter(b)
			if err != nil {
				return err
			}
		}

		return nil

	})

}

func initSimCounter(b *bbolt.Bucket) error {

	cs := common.CountSet{
		Name:   "simidx",
		Size:   1000,
		Offset: 10,
	}

	buf, err := json.Marshal(cs)
	if err != nil {
		return errors.WithStack(err)
	}

	return errors.WithStack(b.Put([]byte(simctr), buf))

}

// get the index from the counter identified by id and save the result to key
func getIndex(id string, key string) (int, error) {

	db, err := bbolt.Open(dbPath, 0600, &bbolt.Options{Timeout: 5 * time.Second})
	if err != nil {
		return -1, errors.WithStack(err)
	}
	defer db.Close()

	var result int
	err = db.Batch(func(tx *bbolt.Tx) error {

		b := tx.Bucket([]byte(simdBucket))

		// first try to get the saved index
		buf := b.Get([]byte(key))
		if buf != nil {
			result, err = strconv.Atoi(string(buf))
			return errors.WithStack(err)
		}

		// if it does not exist, generate a new one
		cs := common.CountSet{
			Name:   id,
			Offset: 10,
			Size:   50000,
		}

		buf = b.Get([]byte(id))
		if buf != nil {
			err := json.Unmarshal(buf, &cs)
			if err != nil {
				return errors.WithStack(err)
			}
		}

		result, cs, err = cs.Add()
		if err != nil {
			return errors.WithStack(err)
		}

		buf, err = json.MarshalIndent(cs, "", "  ")
		if err != nil {
			return errors.WithStack(err)
		}

		// save the counter
		err = b.Put([]byte(id), buf)
		if err != nil {
			return errors.WithStack(err)
		}

		// assocate the new index with the id
		err = b.Put([]byte(key), []byte(fmt.Sprintf("%d", result)))
		if err != nil {
			return errors.WithStack(err)
		}

		return nil

	})

	return result, err
}

// get the inded at key, and remove it from the counter at id, deleting key
// afterwords
func freeIndex(id, key string) error {

	db, err := bbolt.Open(dbPath, 0600, &bbolt.Options{Timeout: 5 * time.Second})
	if err != nil {
		return errors.WithStack(err)
	}
	defer db.Close()

	err = db.Batch(func(tx *bbolt.Tx) error {

		b := tx.Bucket([]byte(simdBucket))
		buf := b.Get([]byte(id))
		if buf == nil {
			return errors.New("key '" + id + "' not found in simd bucket")
		}

		i, err := strconv.Atoi(string(buf))
		if err != nil {
			return errors.WithStack(err)
		}

		var cs common.CountSet
		err = json.Unmarshal(buf, &cs)
		if err != nil {
			return errors.WithStack(err)
		}

		cs.Remove(i)
		if err != nil {
			return errors.WithStack(err)
		}

		buf, err = json.Marshal(cs)
		if err != nil {
			return errors.WithStack(err)
		}

		err = b.Put([]byte(id), buf)
		if err != nil {
			return errors.WithStack(err)
		}

		err = b.Delete([]byte(key))
		if err != nil {
			return errors.WithStack(err)
		}

		return nil

	})

	return nil

}

type SensorMapping struct {
	Name   string
	Local  int
	Global int
}
