package main

import (
	"github.com/pkg/errors"
	"gitlab.com/mergetb/tech/ns"
	"gitlab.com/mergetb/tech/rtnl"
)

func ensureVethPair(
	a, b string,
	bridge string,
	vlan, vni int,
	address string,
	ctxa, ctxb *rtnl.Context,
	nsp *ns.Namespace,
) error {

	ve := rtnl.NewVeth(a, b)
	ve.Info.Veth.PeerNS = uint32(nsp.File.Fd())

	err := ve.Present(ctxa)
	if err != nil {
		return errors.WithStack(err)
	}

	peer, err := rtnl.GetLink(ctxb, b)
	if err != nil {
		return errors.WithStack(err)
	}

	addr, err := rtnl.ParseAddr(address)
	if err != nil {
		return errors.WithStack(err)
	}

	err = peer.EnsureAddr(ctxb, addr)
	if err != nil {
		return errors.WithStack(err)
	}

	err = advertiseVeth(peer, vni)
	if err != nil {
		return err
	}

	err = peer.Up(ctxb)
	if err != nil {
		return errors.WithStack(err)
	}

	br, err := rtnl.GetLink(ctxa, bridge)
	if err != nil {
		return errors.WithStack(err)
	}

	err = ve.SetMaster(ctxa, int(br.Msg.Index))
	if err != nil {
		return errors.WithStack(err)
	}

	err = ve.SetUntagged(ctxa, uint16(vlan), false, true, false)
	if err != nil {
		return errors.WithStack(err)
	}

	return errors.WithStack(ve.Up(ctxa))

}

func connectVtep(
	name string,
	bridge string,
	vlan, vni int,
	ctx *rtnl.Context,
) error {

	vtep, err := rtnl.GetLink(ctx, name)
	if err != nil {
		return errors.WithStack(err)
	}

	br, err := rtnl.GetLink(ctx, bridge)
	if err != nil {
		return errors.WithStack(err)
	}

	err = vtep.SetMaster(ctx, int(br.Msg.Index))
	if err != nil {
		return errors.WithStack(err)
	}

	err = vtep.SetUntagged(ctx, uint16(vlan), false, true, false)
	if err != nil {
		return errors.WithStack(err)
	}

	err = vtep.Up(ctx)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil

}
