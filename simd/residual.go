package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"text/template"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cps/cyphy"
	api "gitlab.com/mergetb/tech/cps/simctl/api"
)

var templateString string = `#include <math.h>

typedef double realtype;

void residual_function(realtype *y, realtype *dy, realtype *r, realtype t)
{
	{{ range $i, $eqtn := .Residuals }}
	// {{ $eqtn.Orig }}
	r[{{ $i }}] = {{ $eqtn.Mapped }};
	{{ end }}
}
`

type ResidEqtn struct {
	Orig   string
	Mapped string
}

type TemplateData struct {
	Residuals []ResidEqtn
}

func buildResidualSharedObject(id string, data *api.SimData) error {

	err := buildResidualFunctionSource(id, data)
	if err != nil {
		return err
	}

	return compileResidualFunction(id)

}

func compileResidualFunction(id string) error {

	src := fmt.Sprintf("%s/%s/resid.c", simsdir, id)
	target := fmt.Sprintf("%s/%s/resid.so", simsdir, id)

	out, err := exec.Command(
		"clang", "-fPIC", "-O2", "-g", "-shared", src, "-o", target,
	).CombinedOutput()

	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"id":      id,
			"message": string(out),
		}).Error("failed to compile residual function")
		return err
	}

	return nil

}

func buildResidualFunctionSource(id string, data *api.SimData) error {

	eqtns, err := extractEquations(data)
	if err != nil {
		return err
	}

	ceqtns, err := extractEquations(data)
	if err != nil {
		return err
	}

	vm := buildVarMap(ceqtns)
	vmjs, err := json.MarshalIndent(vm, "", "  ")
	if err != nil {
		log.WithError(err).Error("failed to serialize varmap")
		return err
	}
	err = ioutil.WriteFile(
		fmt.Sprintf("%s/%s/vm.json", simsdir, id),
		vmjs,
		0644,
	)
	if err != nil {
		log.WithError(err).Error("failed to save varmap")
		return err
	}

	var t TemplateData
	for i, eqtn := range eqtns {
		t.Residuals = append(t.Residuals,
			ResidEqtn{
				Orig:   fmt.Sprintf("(%s) - (%s)", eqtn.Left, eqtn.Right),
				Mapped: fmt.Sprintf("(%s) - (%s)", ceqtns[i].Left, ceqtns[i].Right),
			},
		)
	}

	tmpl, err := template.New("resid").Parse(templateString)
	if err != nil {
		log.WithError(err).Error("failed to parse template string")
		return err
	}

	f, err := os.Create(fmt.Sprintf("%s/%s/resid.c", simsdir, id))
	if err != nil {
		log.WithError(err).Error("failed to create template code file")
		return err
	}

	err = tmpl.Execute(f, t)
	if err != nil {
		log.WithError(err).Error("failed to execute code template")
		return err
	}

	return nil

}

func extractEquations(data *api.SimData) ([]*cyphy.Equation, error) {

	var eqtns []*cyphy.Equation

	for _, x := range data.Equations {
		eqtn, err := cyphy.ParseEquation(x)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"eqtn": x,
			}).Error("failed to parse equation")
			return nil, err
		}
		eqtns = append(eqtns, eqtn)
	}

	return eqtns, nil

}

func buildVarMap(eqtns []*cyphy.Equation) map[string]int {

	counter := new(int)

	vm := make(map[string]int)

	for _, eqtn := range eqtns {

		lhs := eqtn.Left
		rhs := eqtn.Right

		exprVM(lhs, vm, counter)
		exprVM(rhs, vm, counter)

	}

	return vm
}

func exprVM(x *cyphy.Expression, vm map[string]int, counter *int) {

	termVM(x.Left, vm, counter)
	for _, y := range x.Right {
		termVM(y.Term, vm, counter)
	}

}

func termVM(x *cyphy.Term, vm map[string]int, counter *int) {

	factorVM(x.Left, vm, counter)
	for _, y := range x.Right {
		factorVM(y.Factor, vm, counter)
	}

}

func factorVM(x *cyphy.Factor, vm map[string]int, counter *int) {

	baseVM(x.Base, vm, counter)

}

func baseVM(x *cyphy.Value, vm map[string]int, counter *int) {

	if x.Variable != nil {
		if *x.Variable == "t" {
			return
		}
		ctr, ok := vm[*x.Variable]
		if ok {
			*x.Variable = fmt.Sprintf("y[%d]", ctr)
			return
		}
		vm[*x.Variable] = *counter
		*x.Variable = fmt.Sprintf("y[%d]", *counter)
		*counter++
		return
	}

	if x.Intrinsic != nil {
		exprVM(x.Intrinsic.Arg, vm, counter)
		return
	}

	if x.Derivative != nil {
		ctr, ok := vm[*x.Derivative]
		if ok {
			*x.Variable = fmt.Sprintf("dy[%d]", ctr)
			return
		}
		vm[*x.Derivative] = *counter
		// so the c printer dosen't poop out the prime :scream_cat:
		x.Derivative = nil
		x.Variable = new(string)
		*x.Variable = fmt.Sprintf("dy[%d]", *counter)
		*counter++
		return
	}

	if x.Subexpression != nil {
		exprVM(x.Subexpression, vm, counter)
		return
	}

}
