package main

import (
	"io/ioutil"

	"github.com/goccy/go-yaml"
	"github.com/pkg/errors"
)

type Config struct {
	Ifx      string `yaml:"ifx"`
	PhysMTU  int    `yaml:"physMtu"`
	VtepMTU  int    `yaml:"vtepMtu"`
	TunnelIP string `yaml:"tunnelIP"`
	ASN      int    `yaml:"asn"`
}

var (
	config Config
)

func readConfig() error {

	buf, err := ioutil.ReadFile("/etc/simd.yml")
	if err != nil {
		return errors.WithStack(err)
	}

	err = yaml.Unmarshal(buf, &config)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil

}
