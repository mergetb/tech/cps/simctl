package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/cps/simctl/api"
)

func collect(id string) (*api.CollectResponse, error) {

	filename := fmt.Sprintf("%s/%s/run/results.csv", simsdir, id)

	file, err := os.Open(filename)
	if err != nil {
		log.WithError(err).Error("failed to read results file")
		return nil, err
	}
	defer file.Close()

	r := new(api.CollectResponse)
	scanner := bufio.NewScanner(file)

	//header
	scanner.Scan()
	header := scanner.Text()
	r.Header = strings.Split(header, ",")

	for scanner.Scan() {

		line := scanner.Text()
		columns := strings.Split(line, ",")
		for _, s := range columns {
			x, _ := strconv.ParseFloat(s, 64)
			r.Data = append(r.Data, x)
		}

	}

	return r, nil

}
