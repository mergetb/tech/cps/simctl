package main

import (
	"github.com/pkg/errors"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/rtnl"
)

func advertiseVeth(lnk *rtnl.Link, vni int) error {

	err := runtime.EvpnAdvertiseMulticast(
		config.TunnelIP+"/32",
		"localhost",
		config.ASN,
		vni,
		vni,
	)
	if err != nil {
		return errors.WithStack(err)
	}

	err = runtime.EvpnAdvertiseMac(
		config.TunnelIP+"/32",
		"localhost",
		lnk.Info.Address.String(),
		config.ASN,
		vni,
		vni,
	)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil

}
