package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/cps/simctl/api"
	"gitlab.com/mergetb/tech/ns"
	"gitlab.com/mergetb/tech/rtnl"
)

const (
	rundir  = "/var/run/lib/simd"
	simsdir = rundir + "/sims"
)

func init() {

	err := os.MkdirAll(simsdir, 0755)
	if err != nil {
		log.WithError(err).Fatal("failed to ensure runtime dir")
	}

}

func list() (map[string]*api.SimData, error) {

	m := make(map[string]*api.SimData)

	files, err := ioutil.ReadDir(simsdir)
	for _, info := range files {

		data, err := readSimData(info.Name())
		if err != nil {
			log.Error("failed to read simdir %s", info.Name())
			continue
		}

		m[info.Name()] = data

	}

	return m, err

}

func saveSimData(id string, data *api.SimData) error {

	// we assume that the invariant #eqtns = #variables has been checked
	// at a higher level
	data.Config.Size = int32(len(data.Equations) + len(data.Actuators))

	src, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		log.WithError(err).Error("failed to unmarshal data")
		return err
	}

	dir := fmt.Sprintf("%s/%s", simsdir, id)
	err = os.MkdirAll(dir, 0755)
	if err != nil {
		log.WithError(err).Error("failed to ensure simulation runtime dir")
		return err
	}

	fn := fmt.Sprintf("%s/data.json", dir)
	err = ioutil.WriteFile(fn, src, 0655)
	if err != nil {
		log.WithError(err).Error("failed to save sim data file")
		return err
	}

	return nil

}

func deleteSimData(id string) error {

	dir := fmt.Sprintf("%s/%s", simsdir, id)
	err := os.RemoveAll(dir)
	if err != nil {
		log.WithError(err).Error("failed to remove simulation runtime dir")
		return err
	}

	return nil

}

func readSimData(id string) (*api.SimData, error) {

	dir := fmt.Sprintf("%s/%s", simsdir, id)
	fn := fmt.Sprintf("%s/data.json", dir)

	buf, err := ioutil.ReadFile(fn)
	if err != nil {
		log.WithError(err).Error("failed to read sim data")
		return nil, err
	}

	data := new(api.SimData)
	err = json.Unmarshal(buf, data)
	if err != nil {
		log.WithError(err).Error("failed to parse sim data")
	}

	return data, nil

}

func addSensors(id string, sensors []*api.Sensor) error {

	err := provisionSensors(id, sensors)
	if err != nil {
		return err
	}

	data, err := readSimData(id)
	if err != nil {
		return err
	}

	data.Sensors = append(data.Sensors, sensors...)

	err = saveSimData(id, data)
	if err != nil {
		return err
	}

	return nil

}

func removeSensors(id string, sensors []string) error {

	err := destroySensors(id, sensors)
	if err != nil {
		return err
	}

	data, err := readSimData(id)
	if err != nil {
		return err
	}

	var remaining []*api.Sensor
	for _, s := range data.Sensors {
		for _, name := range sensors {
			if s.Name == name {
				continue
			}
		}
		remaining = append(remaining, s)
	}
	data.Sensors = remaining

	err = saveSimData(id, data)
	if err != nil {
		return err
	}

	return nil

}

func addActuators(id string, actuators []*api.Actuator) error {

	err := provisionActuators(id, actuators)
	if err != nil {
		return nil
	}

	data, err := readSimData(id)
	if err != nil {
		return err
	}

	data.Actuators = append(data.Actuators, actuators...)

	err = saveSimData(id, data)
	if err != nil {
		return err
	}

	err = buildResidualSharedObject(id, data)
	if err != nil {
		return err
	}

	return nil

}

func removeActuators(id string, actuators []string) error {

	err := destroyActuators(id, actuators)
	if err != nil {
		return err
	}

	data, err := readSimData(id)
	if err != nil {
		return err
	}

	var remaining []*api.Actuator
	for _, a := range data.Actuators {
		for _, name := range actuators {
			if a.Name == name {
				continue
			}
		}
		remaining = append(remaining, a)
	}
	data.Actuators = remaining

	err = saveSimData(id, data)
	if err != nil {
		return err
	}

	err = buildResidualSharedObject(id, data)
	if err != nil {
		return err
	}

	return nil

}

func addEquations(id string, equations []string) error {

	data, err := readSimData(id)
	if err != nil {
		return err
	}

	data.Equations = append(data.Equations, equations...)

	err = saveSimData(id, data)
	if err != nil {
		return err
	}

	err = buildResidualSharedObject(id, data)
	if err != nil {
		return err
	}

	return nil

}

type EndpointAction int

const (
	ProvisionEndpoint EndpointAction = iota
	DestroyEndpoint
)

func modEndpoints(id string, endpoints []api.Endpoint, action EndpointAction) error {

	if len(endpoints) == 0 {
		return nil
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return errors.WithStack(err)
	}
	defer ctx.Close()

	pctx, err := rtnl.OpenContext(id)
	if err != nil {
		return errors.WithStack(err)
	}
	defer pctx.Close()

	pnsp := &ns.Namespace{Name: id}
	err = pnsp.Open()
	if err != nil {
		return errors.WithStack(err)
	}
	defer pnsp.Close()

	localIndex := fmt.Sprintf("/%s/counters/%s", id, endpoints[0].Type())
	globalIndex := fmt.Sprintf("/%s", endpoints[0].Type())

	simIndex, err := getIndex(simctr, id)
	if err != nil {
		return errors.WithStack(err)
	}

	for _, x := range endpoints {

		localKey := fmt.Sprintf("/%s/%s/%s", id, x.Type(), x.ID())
		globalKey := fmt.Sprintf("/counters/%s/%s", x.Type(), x.ID())

		local, err := getIndex(localIndex, localKey)
		if err != nil {
			return errors.WithStack(err)
		}

		global, err := getIndex(globalIndex, globalKey)
		if err != nil {
			return errors.WithStack(err)
		}

		switch action {

		case ProvisionEndpoint:

			br := fmt.Sprintf("simbr%d", simIndex)
			err = ensureVethPair(
				fmt.Sprintf("%s%d", x.OuterName(), global),
				fmt.Sprintf("%s%d", x.InnerName(), local),
				br,
				local,
				x.Vni(),
				x.IP(),
				ctx,
				pctx,
				pnsp,
			)
			if err != nil {
				return err
			}

			err = connectVtep(
				x.VtepName(),
				br,
				local,
				x.Vni(),
				ctx,
			)
			if err != nil {
				return err
			}

		case DestroyEndpoint:

			ve := &rtnl.Link{
				Info: &rtnl.LinkInfo{
					Name: fmt.Sprintf("%s%d", x.OuterName(), global),
				},
			}
			err := ve.Absent(ctx)
			if err != nil {
				return errors.WithStack(err)
			}

			err = freeIndex(localIndex, localKey)
			if err != nil {
				return errors.WithStack(err)
			}

			err = freeIndex(globalIndex, globalKey)
			if err != nil {
				return errors.WithStack(err)
			}

		}
	}

	return nil

}

func provisionSensors(id string, sensors []*api.Sensor) error {

	var es []api.Endpoint
	for _, x := range sensors {
		es = append(es, x)
	}

	return modEndpoints(id, es, ProvisionEndpoint)

}

func destroySensors(id string, names []string) error {

	var es []api.Endpoint
	for _, x := range names {
		es = append(es, &api.Sensor{Name: x})
	}

	return modEndpoints(id, es, DestroyEndpoint)

}

func provisionActuators(id string, actuators []*api.Actuator) error {

	var es []api.Endpoint
	for _, x := range actuators {
		es = append(es, x)
	}

	return modEndpoints(id, es, ProvisionEndpoint)

}

func destroyActuators(id string, names []string) error {

	var es []api.Endpoint
	for _, x := range names {
		es = append(es, &api.Actuator{Name: x})
	}

	return modEndpoints(id, es, DestroyEndpoint)

}
