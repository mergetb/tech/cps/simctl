package main

import (
	"context"
	"fmt"
	"os"
	"strings"
	"syscall"
	"time"

	"github.com/containerd/containerd"
	"github.com/containerd/containerd/cio"
	"github.com/containerd/containerd/containers"
	"github.com/containerd/containerd/errdefs"
	"github.com/containerd/containerd/images"
	"github.com/containerd/containerd/namespaces"
	"github.com/containerd/containerd/oci"
	"github.com/opencontainers/runtime-spec/specs-go"
	"github.com/pkg/errors"
	"gitlab.com/mergetb/tech/ns"
	"gitlab.com/mergetb/tech/rtnl"
)

var _client *containerd.Client

const (
	containerdSock = "/run/containerd/containerd.sock"
)

func newSimPod(id string) error {

	if id == "" {
		return errors.New("invalid id")
	}

	index, err := getIndex(simctr, id)
	if err != nil {
		return err
	}

	netns, err := ns.Present(id)
	if err != nil {
		return errors.WithStack(err)
	}
	defer netns.Close()

	err = createContainerNS(id)
	if err != nil {
		netns.Absent()
		return err
	}

	err = createControlInterfaces(id, index, netns)
	if err != nil {
		netns.Absent()
		deleteContainerNS(id)
		return err
	}

	err = createSimBridge(index)
	if err != nil {
		netns.Absent() // control interfaces cleaned up by this
		deleteContainerNS(id)
		return err
	}

	err = createSimContainer(id)
	if err != nil {
		netns.Absent()
		deleteContainerNS(id)
		return err
	}

	return nil

}

func delSimPod(id string) error {

	index, err := getIndex(simctr, id)
	if err != nil {
		return err
	}

	err = ns.Absent(id)
	if err != nil {
		return errors.WithStack(err)
	}

	err = deleteSimBridge(index)
	if err != nil {
		return err
	}

	err = deleteSimContainer(id)
	if err != nil {
		return err
	}

	err = deleteContainerNS(id)
	if err != nil {
		return err
	}

	err = ns.Absent(id)
	if err != nil {
		return errors.WithStack(err)
	}

	err = freeIndex(simctr, id)
	if err != nil {
		return err
	}

	return nil

}

func initFS() error {

	err := os.MkdirAll("/var/run/simd", 0755)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil

}

func initializeNetworking() error {

	err := ensureControlBridge()
	if err != nil {
		return err
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return errors.WithStack(err)
	}

	ifx, err := rtnl.GetLink(ctx, config.Ifx)
	if err != nil {
		return errors.WithStack(err)
	}

	err = ifx.SetMtu(ctx, config.PhysMTU)
	if err != nil {
		return errors.WithStack(err)
	}

	lo, err := rtnl.GetLink(ctx, "lo")
	if err != nil {
		return errors.WithStack(err)
	}

	addr, err := rtnl.ParseAddr(config.TunnelIP + "/32")
	if err != nil {
		return errors.WithStack(err)
	}

	err = lo.EnsureAddr(ctx, addr)
	if err != nil {
		return errors.WithStack(err)
	}

	err = ifx.Up(ctx)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil

}

func createContainerNS(id string) error {

	cdc, err := ContainerdClient()
	if err != nil {
		return errors.WithStack(err)
	}

	cns := namespaces.WithNamespace(context.TODO(), id)

	err = cdc.NamespaceService().Create(cns, id, nil)
	if err != nil {
		if errdefs.IsAlreadyExists(err) {
			return nil
		}
		return errors.WithStack(err)
	}

	return nil

}

func deleteContainerNS(id string) error {

	cdc, err := ContainerdClient()
	if err != nil {
		return errors.WithStack(err)
	}

	cns := namespaces.WithNamespace(context.TODO(), id)

	containers, err := cdc.Containers(cns)
	if err != nil {
		return errors.WithStack(err)
	}

	for _, ctr := range containers {

		img, err := ctr.Image(cns)
		if err != nil {
			return errors.WithStack(err)
		}

		err = deleteContainerTask(cns, ctr)
		if err != nil {
			return err
		}

		err = ctr.Delete(cns, containerd.WithSnapshotCleanup)
		if err != nil {
			return err
		}

		err = deleteContainerImg(img.Name(), id)
		if err != nil {
			return err
		}

	}

	err = cdc.NamespaceService().Delete(context.TODO(), id)
	if err != nil {
		if errdefs.IsNotFound(err) {
			return nil
		}
		return errors.WithStack(err)
	}

	return nil

}

func deleteContainerImg(name, namespace string) error {

	c, err := ContainerdClient()
	if err != nil {
		return err
	}

	ctx := namespaces.WithNamespace(context.TODO(), namespace)

	err = c.ImageService().Delete(ctx, name, images.SynchronousDelete())
	if err != nil {
		if errdefs.IsNotFound(err) {
			return nil
		}
		return errors.WithStack(err)
	}

	return nil

}

func createControlInterfaces(id string, index int, nsp *ns.Namespace) error {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return errors.WithStack(err)
	}

	ct := rtnl.NewVeth(fmt.Sprintf("ct%d", index), "ct")
	ct.Info.Veth.PeerNS = uint32(nsp.File.Fd())

	err = ct.Present(ctx)
	if err != nil {
		return errors.WithStack(err)
	}

	ptx, err := rtnl.OpenContext(id)
	if err != nil {
		return errors.WithStack(err)
	}

	peer := &rtnl.Link{
		Info: &rtnl.LinkInfo{Name: "ct"},
	}
	err = peer.Up(ptx)
	if err != nil {
		return errors.WithStack(err)
	}

	br, err := rtnl.GetLink(ctx, "ctl")
	if err != nil {
		return errors.WithStack(err)
	}

	err = ct.SetMaster(ctx, int(br.Msg.Index))
	if err != nil {
		return errors.WithStack(err)
	}

	return errors.WithStack(ct.Up(ctx))

}

func ensureControlBridge() error {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return errors.WithStack(err)
	}

	br := rtnl.NewBridge("ctl")
	err = br.Present(ctx)
	if err != nil {
		return errors.WithStack(err)
	}

	err = br.AddAddrStr(ctx, *svcbrAddr)
	if err != nil && !strings.Contains(err.Error(), "file exists") {
		return errors.WithStack(err)
	}

	err = br.Up(ctx)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil

}

func createSimBridge(index int) error {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return errors.WithStack(err)
	}

	br := rtnl.NewBridge(fmt.Sprintf("simbr%d", index))
	err = br.Present(ctx)
	if err != nil {
		return errors.WithStack(err)
	}

	return br.Up(ctx)

}

func deleteSimBridge(index int) error {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return errors.WithStack(err)
	}

	br, err := rtnl.GetLink(ctx, fmt.Sprintf("simbr%d", index))
	if err != nil {
		return nil
	}

	return errors.WithStack(br.Absent(ctx))

}

func createSimContainer(id string) error {

	cdc, err := ContainerdClient()
	if err != nil {
		return errors.WithStack(err)
	}

	cns := namespaces.WithNamespace(context.TODO(), id)

	deleteContainer(cns, cdc, "cysim")

	_, err = cdc.Pull(cns, *cysimImage, containerd.WithPullUnpack)
	if err != nil {
		return errors.WithStack(err)
	}

	img, err := cdc.GetImage(cns, *cysimImage)
	if err != nil {
		return errors.WithStack(err)
	}

	_, err = cdc.NewContainer(
		cns,
		"cysim",
		containerd.WithImage(img),
		containerd.WithNewSnapshot(fmt.Sprintf("%s-snapshot", id), img),
		containerd.WithNewSpec(
			oci.WithImageConfig(img),
			withSimCNI(id),
			withMounts(id),
		),
	)
	if err != nil {
		if errdefs.IsAlreadyExists(err) {
			return nil
		}
		return errors.WithStack(err)
	}

	return nil

}

func runSim(id string) error {

	stopSim(id)

	cdc, err := ContainerdClient()
	if err != nil {
		return errors.WithStack(err)
	}

	cns := namespaces.WithNamespace(context.TODO(), id)

	ctr, err := cdc.LoadContainer(cns, "cysim")
	if err != nil {
		return errors.WithStack(err)
	}

	return createTask(cns, ctr, id, "cysim")

}

func createTask(
	cns context.Context, ctr containerd.Container, ns, name string) error {

	// ensure container logfile directory exists
	cnsDir := fmt.Sprintf("/var/log/containers/%s", ns)
	err := os.MkdirAll(cnsDir, 0755)
	if err != nil {
		return errors.WithStack(err)
	}

	// create a task object for the container
	logfile := fmt.Sprintf("%s/%s", cnsDir, name)
	task, err := ctr.NewTask(cns, cio.LogFile(logfile))
	if err != nil {
		return errors.WithStack(err)
	}

	// start the container task
	err = task.Start(cns)
	if err != nil {
		return errors.WithStack(err)
	}

	// wait for task to actually start
	err = waitForProcessStart(cns, task)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil

}

func stopSim(id string) error {

	cdc, err := ContainerdClient()
	if err != nil {
		return errors.WithStack(err)
	}

	cns := namespaces.WithNamespace(context.TODO(), id)

	ctr, err := cdc.LoadContainer(cns, "cysim")
	if err != nil {
		return errors.WithStack(err)
	}

	return deleteContainerTask(cns, ctr)

}

func waitForProcessStart(ctx context.Context, task containerd.Task) error {

	for i := 0; i < 10; i++ {

		s, err := task.Status(ctx)
		if err != nil {
			return errors.WithStack(err)
		}

		if s.Status == containerd.Running {
			return nil
		}

		time.Sleep(250 * time.Millisecond)

	}

	return errors.New("ctr task failed to start")

}

func deleteSimContainer(id string) error {

	cdc, err := ContainerdClient()
	if err != nil {
		return errors.WithStack(err)
	}

	cns := namespaces.WithNamespace(context.TODO(), id)

	return errors.WithStack(deleteContainer(cns, cdc, id))

}

func deleteContainer(
	cns context.Context, cdc *containerd.Client, id string,
) error {

	ctr, err := getContainer(cdc, cns, id)
	if err != nil {
		return err
	}
	if ctr == nil {
		return nil
	}

	err = deleteContainerTask(cns, ctr)
	if err != nil {
		return err
	}

	img, err := ctr.Image(cns)
	if err != nil {
		return errors.WithStack(err)
	}
	err = cdc.ImageService().Delete(cns, img.Name(), images.SynchronousDelete())
	if err != nil {
		return errors.WithStack(err)
	}

	err = ctr.Delete(cns, containerd.WithSnapshotCleanup)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil

}

func getContainer(cdc *containerd.Client, cns context.Context, name string) (
	containerd.Container, error) {

	cs, err := cdc.Containers(cns)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	for _, c := range cs {
		if name == c.ID() {
			return c, nil
		}
	}

	return nil, nil

}

func deleteContainerTask(
	cns context.Context, ctr containerd.Container) error {

	task, err := ctr.Task(cns, cio.Load)
	if err != nil {
		if strings.HasPrefix(err.Error(), "no running task found") {
			return nil
		}
		return errors.WithStack(err)
	}

	s, err := task.Status(cns)
	if err != nil {
		return errors.WithStack(err)
	}
	if s.Status == containerd.Running {

		exit, err := task.Wait(cns)
		if err != nil {
			return errors.WithStack(err)
		}

		err = task.Kill(cns, syscall.SIGKILL)
		if err != nil {
			return errors.WithStack(err)
		}

		status := <-exit
		_, _, err = status.Result()
		if err != nil {
			return errors.WithStack(err)
		}
	}

	_, err = task.Delete(cns)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil

}

func withSimCNI(netns string) oci.SpecOpts {

	return func(
		ctx context.Context,
		client oci.Client,
		ctr *containers.Container,
		spec *oci.Spec,
	) error {

		for i, x := range spec.Linux.Namespaces {
			if x.Type == "network" {
				spec.Linux.Namespaces[i] = specs.LinuxNamespace{
					Type: "network",
					Path: nspath(netns),
				}
			}
		}

		return nil

	}

}

func withMounts(id string) oci.SpecOpts {

	return func(
		ctx context.Context,
		client oci.Client,
		ctr *containers.Container,
		spec *oci.Spec,
	) error {

		spec.Mounts = append(spec.Mounts, specs.Mount{
			Source:      fmt.Sprintf("%s/%s", simsdir, id),
			Destination: "/var/run/simd",
			Options:     []string{"rbind", "ro"},
			Type:        "bind",
		})

		return nil

	}

}

func nspath(name string) string {

	return fmt.Sprintf("/var/run/netns/%s", name)

}

func ContainerdClient() (*containerd.Client, error) {

	if _client != nil {
		return _client, nil
	}

	var err error
	_client, err = containerd.New(containerdSock)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return _client, nil

}
