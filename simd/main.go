package main

import (
	"context"
	"flag"
	"net"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/cps/simctl/api"
	"google.golang.org/grpc"
)

var (
	endpoint = flag.String(
		"endpoint", "0.0.0.0:6000", "listening endpoint",
	)
	simctlImage = flag.String(
		"simctlImage", "docker.io/mergetb/simctl:latest", "simctl image to use",
	)
	svcbrAddr = flag.String(
		"svcbrAddr", "192.168.1.1/24", "service bridge address",
	)
	cysimImage = flag.String(
		"cysimImage",
		"docker.io/mergetb/cysim:latest",
		"cysim container image to use",
	)
	debug = flag.Bool(
		"debug",
		false,
		"enable debugging",
	)
)

func main() {

	flag.Parse()
	if *debug {
		log.SetLevel(log.DebugLevel)
	}

	err := readConfig()
	if err != nil {
		log.Fatalf("failed to read config: %v", err)
	}

	log.Infof("simd version %s", api.Version)

	err = initializeNetworking()
	if err != nil {
		log.Fatalf("network initialization failed: %v", err)
	}

	err = initFS()
	if err != nil {
		log.Fatalf("file system initialization failed: %v", err)
	}

	err = initDb()
	if err != nil {
		log.Fatalf("failed to initialize DB: %v", err)
	}

	l, err := net.Listen("tcp", *endpoint)
	if err != nil {
		log.Fatal(err)
	}

	gs := grpc.NewServer()
	api.RegisterSimdServer(gs, new(simd))
	log.Infof("listening on: %s", *endpoint)
	gs.Serve(l)

}

type simd struct{}

// initialization -------------------------------------------------------------

func (s *simd) Init(
	ctx context.Context, r *api.InitRequest,
) (*api.InitResponse, error) {

	log.Debugf("init: %+v", *r)

	err := saveSimData(r.Id, r.Data)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	err = newSimPod(r.Id)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	err = buildResidualSharedObject(r.Id, r.Data)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	return &api.InitResponse{}, nil

}

func (s *simd) Destroy(
	ctx context.Context, r *api.DestroyRequest,
) (*api.DestroyResponse, error) {

	log.Debugf("destroy: %+v", *r)

	err := delSimPod(r.Id)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	err = deleteSimData(r.Id)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	return &api.DestroyResponse{}, nil

}

// list -----------------------------------------------------------------------

func (s *simd) List(
	ctx context.Context, r *api.ListRequest,
) (*api.ListResponse, error) {

	sims, err := list()
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	return &api.ListResponse{
		Sims: sims,
	}, nil

}

// sensors --------------------------------------------------------------------

func (s *simd) AddSensors(
	ctx context.Context, r *api.AddSensorRequest,
) (*api.AddSensorResponse, error) {

	log.Debugf("add sensor: %+v", *r)

	err := addSensors(r.Id, r.Sensors)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	return &api.AddSensorResponse{}, nil

}

func (s *simd) RemoveSensors(
	ctx context.Context, r *api.RemoveSensorRequest,
) (*api.RemoveSensorResponse, error) {

	log.Debugf("remove sensor: %+v", *r)

	err := removeSensors(r.Id, r.Sensors)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	return &api.RemoveSensorResponse{}, nil

}

// actuators ------------------------------------------------------------------

func (s *simd) AddActuators(
	ctx context.Context, r *api.AddActuatorRequest,
) (*api.AddActuatorResponse, error) {

	log.Debugf("add actuator: %+v", *r)

	err := addActuators(r.Id, r.Actuators)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	return &api.AddActuatorResponse{}, nil

}

func (s *simd) RemoveActuators(
	ctx context.Context, r *api.RemoveActuatorRequest,
) (*api.RemoveActuatorResponse, error) {

	log.Debugf("remove actuator: %+v", *r)

	err := removeActuators(r.Id, r.Actuators)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	return &api.RemoveActuatorResponse{}, nil

}

// equations ------------------------------------------------------------------

func (s *simd) AddEquations(
	ctx context.Context, r *api.AddEquationRequest,
) (*api.AddEquationResponse, error) {

	log.Debugf("add equations: %+v", *r)

	err := addEquations(r.Id, r.Equations)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	return &api.AddEquationResponse{}, nil

}

func (s *simd) RemoveEquations(
	ctx context.Context, r *api.RemoveEquationRequest,
) (*api.RemoveEquationResponse, error) {

	log.Debugf("remove equations: %+v", *r)

	//TODO
	return nil, nil

}

// simulation control ---------------------------------------------------------

func (s *simd) Start(
	ctx context.Context, r *api.StartRequest,
) (*api.StartResponse, error) {

	log.Debugf("start: %+v", *r)

	err := runSim(r.Id)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	return &api.StartResponse{}, nil

}

func (s *simd) Stop(
	ctx context.Context, r *api.StopRequest,
) (*api.StopResponse, error) {

	log.Debugf("stop: %+v", *r)

	err := stopSim(r.Id)
	if err != nil {
		log.Errorf("%+v", err)
		return nil, err
	}

	return &api.StopResponse{}, nil

}

// data collection ------------------------------------------------------------

func (s *simd) Collect(
	ctx context.Context, r *api.CollectRequest,
) (*api.CollectResponse, error) {

	log.Debugf("collect: %+v", *r)

	return collect(r.Id)

}
