package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
	api "gitlab.com/mergetb/tech/cps/simctl/api"
	"google.golang.org/grpc"
)

var (
	endpoint string
	tw       = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
)

func main() {

	log.SetFlags(0)

	root := cobra.Command{
		Use:   "simc",
		Short: "Simd control client",
	}

	version := &cobra.Command{
		Use:   "version",
		Short: "print version",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			log.Printf(api.Version)
		},
	}
	root.PersistentFlags().StringVar(
		&endpoint, "endpoint", "localhost:6000", "simd endpoint")
	root.AddCommand(version)

	init := &cobra.Command{
		Use:   "init <id> <config.json>",
		Short: "Initialize a simulation",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			doInit(args[0], args[1])
		},
	}
	root.AddCommand(init)

	start := &cobra.Command{
		Use:   "start <id>",
		Short: "Start a simulation",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doStart(args[0])
		},
	}
	root.AddCommand(start)

	stop := &cobra.Command{
		Use:   "stop <id>",
		Short: "Stop a simulation",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doStop(args[0])
		},
	}
	root.AddCommand(stop)

	collect := &cobra.Command{
		Use:   "collect <id>",
		Short: "Collect simulation data",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doCollect(args[0])
		},
	}
	root.AddCommand(collect)

	list := &cobra.Command{
		Use:   "list <id>",
		Short: "List simulations",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			doList()
		},
	}
	root.AddCommand(list)

	root.Execute()

}

func doInit(id, configfile string) {

	conn, cli := connect()
	defer conn.Close()

	src, err := ioutil.ReadFile(configfile)
	if err != nil {
		log.Fatal(err)
	}

	data := new(api.SimData)
	err = json.Unmarshal(src, data)
	if err != nil {
		log.Fatal(err)
	}

	_, err = cli.Init(context.TODO(), &api.InitRequest{
		Id:   id,
		Data: data,
	})
	if err != nil {
		log.Fatal(err)
	}

}

func doStart(id string) {

	conn, cli := connect()
	defer conn.Close()

	_, err := cli.Start(context.TODO(), &api.StartRequest{Id: id})
	if err != nil {
		log.Fatal(err)
	}

}

func doStop(id string) {

	conn, cli := connect()
	defer conn.Close()

	_, err := cli.Stop(context.TODO(), &api.StopRequest{Id: id})
	if err != nil {
		log.Fatal(err)
	}

}

func connect() (*grpc.ClientConn, api.SimdClient) {

	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("simd connection failed: %v", err)
	}
	client := api.NewSimdClient(conn)

	return conn, client

}

func doCollect(id string) {

	conn, cli := connect()
	defer conn.Close()

	resp, err := cli.Collect(context.TODO(), &api.CollectRequest{Id: id})
	if err != nil {
		log.Fatal(err)
	}

	f, err := os.Create("results.csv")
	if err != nil {
		log.Fatal("failed to create output file: %v", err)
	}

	for _, column := range resp.Header {

		fmt.Fprintf(f, "%s,", column)

	}

	n := len(resp.Header) - 1

	for i, value := range resp.Data {

		if i%n == 0 {
			fmt.Fprintf(f, "\n")
		}

		fmt.Fprintf(f, "%f,", value)

	}
	fmt.Fprintf(f, "\n")

}

func doList() {

	conn, cli := connect()
	defer conn.Close()

	resp, err := cli.List(context.TODO(), &api.ListRequest{})
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(tw, "NAME\tEQUATIONS\tSENSORS\tACTUATORS\n")

	for name, data := range resp.Sims {

		fmt.Fprintf(tw, "%s\t%d\t%d\t%d\n",
			name,
			len(data.Equations),
			len(data.Sensors),
			len(data.Actuators),
		)

	}

	tw.Flush()

}
