package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	api "gitlab.com/mergetb/tech/cps/simctl/api"
	"google.golang.org/grpc"
)

var endpoint string

func main() {

	log.SetFlags(0)

	root := cobra.Command{
		Use:   "simctl",
		Short: "Simulation control client",
	}

	version := &cobra.Command{
		Use:   "version",
		Short: "print version",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			log.Printf(api.Version)
		},
	}
	root.PersistentFlags().StringVar(
		&endpoint, "endpoint", "simctld:6000", "simd endpoint")
	root.AddCommand(version)

	start := &cobra.Command{
		Use:   "start <id>",
		Short: "Start a simulation",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doStart(args[0])
		},
	}
	root.AddCommand(start)

	stop := &cobra.Command{
		Use:   "stop <id>",
		Short: "Stop a simulation",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doStop(args[0])
		},
	}
	root.AddCommand(stop)

	collect := &cobra.Command{
		Use:   "collect <id>",
		Short: "Collect simulation data",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			doCollect(args[0])
		},
	}
	root.AddCommand(collect)

	root.Execute()

}

func doStart(id string) {

	conn, cli := connect()
	defer conn.Close()

	_, err := cli.Start(context.TODO(), &api.StartRequest{Id: id})
	if err != nil {
		log.Fatal(err)
	}

}

func doStop(id string) {

	conn, cli := connect()
	defer conn.Close()

	_, err := cli.Stop(context.TODO(), &api.StopRequest{Id: id})
	if err != nil {
		log.Fatal(err)
	}

}

func connect() (*grpc.ClientConn, api.SimctldClient) {

	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("simd connection failed: %v", err)
	}
	client := api.NewSimctldClient(conn)

	return conn, client

}

func doCollect(id string) {

	conn, cli := connect()
	defer conn.Close()

	resp, err := cli.Collect(context.TODO(), &api.CollectRequest{Id: id})
	if err != nil {
		log.Fatal(err)
	}

	f, err := os.Create("results.csv")
	if err != nil {
		log.Fatal("failed to create output file: %v", err)
	}

	for _, column := range resp.Header {

		fmt.Fprintf(f, "%s,", column)

	}

	n := len(resp.Header) - 1

	for i, value := range resp.Data {

		if i%n == 0 {
			fmt.Fprintf(f, "\n")
		}

		fmt.Fprintf(f, "%f,", value)

	}
	fmt.Fprintf(f, "\n")

}
