module gitlab.com/mergetb/tech/cps/simctl

go 1.13

require (
	github.com/containerd/containerd v1.2.2
	github.com/goccy/go-yaml v1.7.1
	github.com/golang/protobuf v1.3.5
	github.com/opencontainers/runtime-spec v1.0.1
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.7
	gitlab.com/mergetb/tech/cogs v0.7.6-0.20200503224008-14feefc57d96
	gitlab.com/mergetb/tech/cps/cyphy v0.0.0-20200406160429-33377d8a34d8
	gitlab.com/mergetb/tech/ns v0.0.0-20200506235849-981176c3191d
	gitlab.com/mergetb/tech/rtnl v0.1.10-0.20200607224605-2a8dfd8e77b7
	go.etcd.io/bbolt v1.3.3
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	google.golang.org/grpc v1.28.0
)

//replace gitlab.com/mergetb/tech/cps/cyphy => /space/cps/cyphy

// replace gitlab.com/mergetb/tech/ns => /space/cps/ns

// replace gitlab.com/mergetb/tech/rtnl => /space/cps/rtnl

//replace gitlab.com/mergetb/tech/cogs => /space/cps/cogs
