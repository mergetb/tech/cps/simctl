package simctl

import (
	"fmt"
)

type Endpoint interface {
	Type() string
	ID() string
	InnerName() string
	OuterName() string
	VtepName() string
	Vni() int
	IP() string
}

func (s *Sensor) Type() string      { return "sensor" }
func (s *Sensor) ID() string        { return s.Name }
func (s *Sensor) InnerName() string { return "sx" }
func (s *Sensor) OuterName() string { return "sen" }
func (s *Sensor) VtepName() string  { return fmt.Sprintf("vtep%d", s.Vni()) }
func (s *Sensor) Vni() int          { return int(s.Vxlan) }
func (s *Sensor) IP() string        { return s.Address }

func (a *Actuator) Type() string      { return "actuator" }
func (a *Actuator) ID() string        { return a.Name }
func (a *Actuator) InnerName() string { return "ax" }
func (a *Actuator) OuterName() string { return "act" }
func (a *Actuator) VtepName() string  { return fmt.Sprintf("vtep%d", a.Vni()) }
func (a *Actuator) Vni() int          { return int(a.Vxlan) }
func (a *Actuator) IP() string        { return a.Address }
