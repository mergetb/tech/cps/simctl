package main

import (
	"context"
	"flag"
	"io/ioutil"
	"net"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/cps/simctl/api"
	"google.golang.org/grpc"
)

var (
	endpoint = flag.String("endpoint", "0.0.0.0:6000", "listening endpoint")
)

func main() {

	flag.Parse()

	log.Infof("simd version %s", api.Version)

	l, err := net.Listen("tcp", *endpoint)
	if err != nil {
		log.Fatal(err)
	}

	gs := grpc.NewServer()
	api.RegisterSimctldServer(gs, new(simctld))
	log.Infof("listening on: %s", *endpoint)
	gs.Serve(l)

}

type simctld struct{}

// initialization -------------------------------------------------------------

func (s *simctld) Init(
	ctx context.Context, r *api.InitRequest,
) (*api.InitResponse, error) {

	id, err := getId()
	if err != nil {
		return nil, err
	}
	r.Id = id

	conn, cli := connect()
	defer conn.Close()
	return cli.Init(ctx, r)

}

// sensors --------------------------------------------------------------------

func (s *simctld) AddSensors(
	ctx context.Context, r *api.AddSensorRequest,
) (*api.AddSensorResponse, error) {

	id, err := getId()
	if err != nil {
		return nil, err
	}
	r.Id = id

	conn, cli := connect()
	defer conn.Close()
	return cli.AddSensors(ctx, r)

}

func (s *simctld) RemoveSensors(
	ctx context.Context, r *api.RemoveSensorRequest,
) (*api.RemoveSensorResponse, error) {

	id, err := getId()
	if err != nil {
		return nil, err
	}
	r.Id = id

	conn, cli := connect()
	defer conn.Close()
	return cli.RemoveSensors(ctx, r)

}

// actuators ------------------------------------------------------------------

func (s *simctld) AddActuators(
	ctx context.Context, r *api.AddActuatorRequest,
) (*api.AddActuatorResponse, error) {

	id, err := getId()
	if err != nil {
		return nil, err
	}
	r.Id = id

	conn, cli := connect()
	defer conn.Close()
	return cli.AddActuators(ctx, r)

}

func (s *simctld) RemoveActuators(
	ctx context.Context, r *api.RemoveActuatorRequest,
) (*api.RemoveActuatorResponse, error) {

	id, err := getId()
	if err != nil {
		return nil, err
	}
	r.Id = id

	conn, cli := connect()
	defer conn.Close()
	return cli.RemoveActuators(ctx, r)

}

// equations ------------------------------------------------------------------

func (s *simctld) AddEquations(
	ctx context.Context, r *api.AddEquationRequest,
) (*api.AddEquationResponse, error) {

	id, err := getId()
	if err != nil {
		return nil, err
	}
	r.Id = id

	conn, cli := connect()
	defer conn.Close()
	return cli.AddEquations(ctx, r)

}

func (s *simctld) RemoveEquations(
	ctx context.Context, r *api.RemoveEquationRequest,
) (*api.RemoveEquationResponse, error) {

	id, err := getId()
	if err != nil {
		return nil, err
	}
	r.Id = id

	conn, cli := connect()
	defer conn.Close()
	return cli.RemoveEquations(ctx, r)

}

// simulation control ---------------------------------------------------------

func (s *simctld) Start(
	ctx context.Context, r *api.StartRequest,
) (*api.StartResponse, error) {

	id, err := getId()
	if err != nil {
		return nil, err
	}
	r.Id = id

	conn, cli := connect()
	defer conn.Close()
	return cli.Start(ctx, r)

}

func (s *simctld) Stop(
	ctx context.Context, r *api.StopRequest,
) (*api.StopResponse, error) {

	id, err := getId()
	if err != nil {
		return nil, err
	}
	r.Id = id

	conn, cli := connect()
	defer conn.Close()

	return cli.Stop(context.TODO(), r)

}

// data collection ------------------------------------------------------------

func (s *simctld) Collect(
	ctx context.Context, r *api.CollectRequest,
) (*api.CollectResponse, error) {

	id, err := getId()
	if err != nil {
		return nil, err
	}
	r.Id = id

	conn, cli := connect()
	defer conn.Close()

	return cli.Collect(context.TODO(), r)

}

func getId() (string, error) {

	buf, err := ioutil.ReadFile("/var/run/simid")
	if err != nil {
		log.WithError(err).Error("failed to read simid")
		return "", err
	}

	return string(buf), nil

}

func connect() (*grpc.ClientConn, api.SimdClient) {

	conn, err := grpc.Dial(*endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("simd connection failed: %v", err)
	}
	client := api.NewSimdClient(conn)

	return conn, client

}
