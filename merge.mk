BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) go build -ldflags=$(LDFLAGS) -o $@ $(dir $<)*.go
endef

define go-build-file
	$(call build-slug,go)
	$(QUIET) go build -ldflags=$(LDFLAGS) -o $@ $<
endef

define go-build-test
	$(call build-slug,go-test)
	$(QUIET) go test -o $@ -c $<
endef

define protoc-build
	$(call build-slug,protoc)
	$(QUIET) GOBIN=`pwd`/.tools go install github.com/golang/protobuf/protoc-gen-go
	$(QUIET) rm -f $@
	$(QUIET) PATH=./.tools:$$PATH protoc \
		-I . \
		-I ./$(dir $@) \
		./$< \
		--go_out=plugins=grpc:.
	$(QUIET) mv `find -name $(notdir $@) | grep -v vendor` $@
endef

define docker-build
	$(call build-slug,docker)
	$(QUIET) docker build ${BUILD_ARGS} ${1} $(DOCKER_QUIET) -f $< -t $(@):$(TAG) .
	$(if ${PUSH},$(call docker-push))
endef

define docker-push
	$(call build-slug,push)
	@docker push $(@):$(TAG)
endef
